//
//  AppDelegate.h
//  alarmapp
//
//  Created by codemac-di on 03/09/16.
//  Copyright © 2016 codemac-di. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "addalarmViewController.h"  

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
   
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(assign,nonatomic)NSInteger user1;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

