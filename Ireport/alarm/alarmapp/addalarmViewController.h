//
//  addalarmViewController.h
//  alarmapp
//
//  Created by codemac-di on 03/09/16.
//  Copyright © 2016 codemac-di. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface addalarmViewController : UIViewController{
    NSInteger hour,minute;
    BOOL switchstate;
    
}

@property (strong, nonatomic) IBOutlet UIDatePicker *datepicker;
@property (strong,nonatomic)NSDate *pickertime;
@property (strong, nonatomic) IBOutlet UISwitch *sw;
@property (strong,nonatomic)NSArray *alarmnum1;
@property (strong,nonatomic)NSArray *obj;
@property (assign,nonatomic)NSNumber *num;
-(NSNumber *)fetch;

- (IBAction)btndone:(id)sender;


- (IBAction)sw:(id)sender;


@end
