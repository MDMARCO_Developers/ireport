//
//  ViewController.h
//  alarmapp
//
//  Created by codemac-di on 03/09/16.
//  Copyright © 2016 codemac-di. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "addalarmViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong,nonatomic)NSArray *obj;
@property(strong,nonatomic)NSMutableArray *obj1;
@property (strong, nonatomic)NSMutableArray *alarmnum;
@property (strong, nonatomic)NSMutableArray *alarmtimehour;
@property (strong, nonatomic)NSMutableArray *alarmtimemin;
@property (strong, nonatomic)NSMutableArray *switchstate;
@property (strong,nonatomic)NSIndexPath *indexpath1;
@property (assign,nonatomic) BOOL state;
@property (strong,nonatomic)  UILabel *lbltime1;
@property (strong, nonatomic)  UILabel *lbltime2;
@property (strong, nonatomic)  UISwitch *sw1;
@property (strong,nonatomic)UIAlertController *alert;

- (IBAction)swaction:(id)sender;
@end

