//
//  alarmTableViewCell.h
//  alarmapp
//
//  Created by codemac-di on 03/09/16.
//  Copyright © 2016 codemac-di. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface alarmTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblalarm;
@property (strong, nonatomic) IBOutlet UILabel *lbltime1;


@property (strong, nonatomic) IBOutlet UILabel *lbltime;
@property (strong, nonatomic) IBOutlet UISwitch *sw;



- (IBAction)sw:(id)sender;



@end
