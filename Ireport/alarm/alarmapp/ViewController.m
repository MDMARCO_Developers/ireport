//
//  ViewController.m
//  alarmapp
//
//  Created by codemac-di on 03/09/16.
//  Copyright © 2016 codemac-di. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    CGFloat width;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"%ld",(long)self.state);
   // width=[UIDv ]
    //coredata fetch
    
    AppDelegate *app=[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[app managedObjectContext];
    NSFetchRequest *req=[[NSFetchRequest alloc]initWithEntityName:@"Alarm"];
    
    self.obj=[context executeFetchRequest:req error:nil];
    self.obj1=[self.obj mutableCopy];
    self.alarmnum=[self.obj1 valueForKey:@"alarmnum"];
    self.switchstate=[self.obj1 valueForKey:@"switchstate"];
    
    NSLog(@"switchstate in didload=%@",self.switchstate);
    NSLog(@"ALARMNUM%@",self.alarmnum.lastObject);
    //self.alarmtimehour=[self.obj1 valueForKey:@"alarmtimehour"];
    self.alarmtimehour=[[NSMutableArray alloc]initWithArray:[self.obj1 valueForKey:@"alarmtimehour"]];
     NSLog(@"switchstate in didload=%@",self.alarmtimehour);
    self.alarmtimemin=[self.obj1 valueForKey:@"alarmtimemin"];

      [self.table reloadData];
    
   }

-(void)viewWillAppear:(BOOL)animated{
    
    AppDelegate *app=[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[app managedObjectContext];
    NSFetchRequest *req=[[NSFetchRequest alloc]initWithEntityName:@"Alarm"];
    
    self.obj=[context executeFetchRequest:req error:nil];
    self.obj1=[self.obj mutableCopy];
    self.alarmnum=[self.obj1 valueForKey:@"alarmnum"];
    self.switchstate=[self.obj1 valueForKey:@"switchstate"];
    
    NSLog(@"switchstate in didload=%@",self.switchstate);
    NSLog(@"ALARMNUM%@",self.alarmnum.lastObject);
    self.alarmtimehour=[self.obj1 valueForKey:@"alarmtimehour"];
    self.alarmtimemin=[self.obj1 valueForKey:@"alarmtimemin"];

    NSLog(@"WILLAPPEAR");

    [self.table reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark tableview DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.alarmtimehour.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cell";
    UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    else{
        NSArray *cellSubs=cell.contentView.subviews;
        for(int i=0;i<[cellSubs count];i++){
            [[cellSubs objectAtIndex:i]removeFromSuperview];
            
        }
    }
    
    UILabel *lblalarm=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 50, 15)];
    lblalarm.textAlignment=NSTextAlignmentLeft;
    lblalarm.text=@"Alarm";
    [cell addSubview:lblalarm];
    
    self.lbltime1=[[UILabel alloc]initWithFrame:CGRectMake(10, 40, 50, 15)];
    self.lbltime1.textAlignment=NSTextAlignmentLeft;
    self.lbltime1.text=@"Time";
    [cell addSubview:self.lbltime1];
    
    self.lbltime2=[[UILabel alloc] initWithFrame:CGRectMake(55, 40, 60, 15)];
    self.lbltime2.textAlignment=NSTextAlignmentLeft;
    self.lbltime2.text=[NSString stringWithFormat:@"%@:%@",[self.alarmtimehour objectAtIndex:indexPath.row],[self.alarmtimemin objectAtIndex:indexPath.row]];
    [cell addSubview:self.lbltime2];
    
    self.sw1=[[UISwitch alloc]initWithFrame:CGRectMake(130, 30, 51, 31)];
    NSNumber *str=[self.switchstate objectAtIndex:indexPath.row];
    NSInteger st=[str intValue];
    NSLog(@"table%ld",(long)st);
    
    //  [self.sw1 setOn:YES animated:YES];
    
    if(self.sw1.on&&st==0){
        [self.sw1 setOn:NO animated:YES];
    }
    else if(st==1){
        [self.sw1 setOn:YES animated:YES];
    }
    [self.sw1 addTarget:self action:@selector(swaction:) forControlEvents:UIControlEventValueChanged];
    [cell addSubview:self.sw1];
    
    
    return cell;
}



//-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
// 
// return YES;
// }

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //alert
    
    self.alert=[UIAlertController alertControllerWithTitle:nil message:@"are u sure???" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OK=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self ok:indexPath];
    }];
    [self.alert addAction:OK];
    
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [self.alert addAction:cancel];
    
    [self presentViewController:self.alert animated:YES completion:nil];
    
    
    //    NSLog(@"%ld",(long)indexPath.row);
    //    NSLog(@"%@",[self.alarmnum objectAtIndex:indexPath.row]);
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    UINavigationController *nav=segue.destinationViewController;
    addalarmViewController *add=[nav childViewControllers].firstObject;
    add.num=self.alarmnum.lastObject;
}

-(IBAction)swaction:(id)sender {
    
    //UISwitch *swaction=sender;
    CGPoint swposition=[sender convertPoint:CGPointZero toView:self.table];
    NSIndexPath *indexpath=[self.table indexPathForRowAtPoint:swposition];
    
       AppDelegate *app=[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[app managedObjectContext];
    NSEntityDescription *entitydesc=[NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:context];
    NSFetchRequest *req=[[NSFetchRequest alloc]init];
    [req setEntity:entitydesc];
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"alarmnum=%@",[self.alarmnum objectAtIndex:indexpath.row]];
    [req setPredicate:pred];
    NSArray *obj=[context executeFetchRequest:req error:nil];
    //    NSLog(@"%@",obj);
    
    NSNumber *str=[self.switchstate objectAtIndex:indexpath.row];
    NSInteger st=[str intValue];
    NSLog(@"%ld",(long)st);
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        if(self.sw1.on&&st==0){
            
            NSInteger stateon=1;
            //[self.sw1 setOn:YES];
            NSNumber *switchstate2=[NSNumber numberWithBool:stateon];
            [obj setValue:switchstate2 forKey:@"switchstate"];
            
        }
        else if(st==1){
            
            //        [self.sw1 setOn:NO];
            NSInteger stateoff=0;
            NSNumber *switchstate1=[NSNumber numberWithBool:stateoff];
            //            NSLog(@"%@",switchstate1);
            [obj setValue:switchstate1 forKey:@"switchstate"];
            //            NSLog(@"%@",[obj valueForKey:@"switchstate"]);
            //            NSLog(@"%@",self.switchstate);
            
        }
        
        
        NSError *error;
        [context save:&error];
    });
    
    [self viewDidLoad];
    
}
-(void)ok:(NSIndexPath *)indexpath {
    
    
    AppDelegate *app=[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[app managedObjectContext];
    NSFetchRequest *req=[[NSFetchRequest alloc]init];
    NSEntityDescription *entitydesc=[NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:context];
    [req setEntity:entitydesc];
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"alarmnum=%@",[self.alarmnum objectAtIndex:indexpath.row]];
    
    NSLog(@" object at index = %@",[self.alarmnum objectAtIndex:indexpath.row]);
    
    NSLog(@"alarm time = %@ hour = %@ switchstate = %@",self.alarmnum,self.alarmtimehour,self.switchstate);
    [req setPredicate:pred];
    NSArray *result=[context executeFetchRequest:req error:nil];
    
    for(NSManagedObject *objects in result)
    {
        
        [context deleteObject:objects];
        [context save:nil];
       
    }
    
    [self.table reloadData];
    [self.alarmnum removeObject:[self.alarmnum objectAtIndex:indexpath.row]];
    [self.alarmtimehour removeObject:[self.alarmtimehour objectAtIndex:indexpath.row]];
    [self.alarmtimemin removeObject:[self.alarmtimemin objectAtIndex:indexpath.row]];
    [self.switchstate removeObject:[self.switchstate objectAtIndex:indexpath.row]];
    
//    [self.alarmnum removeObjectAtIndex:indexpath.row];
//    [self.alarmtimehour removeObjectAtIndex:indexpath.row];
//    [self.alarmtimemin removeObjectAtIndex:indexpath.row];
//    [self.switchstate removeObjectAtIndex:indexpath.row];
    [self.table deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.table reloadData];

    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
