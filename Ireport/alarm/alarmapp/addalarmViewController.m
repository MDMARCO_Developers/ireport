//
//  addalarmViewController.m
//  alarmapp
//
//  Created by codemac-di on 03/09/16.
//  Copyright © 2016 codemac-di. All rights reserved.
//

#import "addalarmViewController.h"

@interface addalarmViewController ()

@end
@implementation addalarmViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
  
    switchstate=@"on";
    
    UIUserNotificationType types = (UIUserNotificationType) (UIUserNotificationTypeBadge |
                                                             UIUserNotificationTypeSound | UIUserNotificationTypeAlert);
    UIUserNotificationSettings *mySettings =
    [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    UINavigationController *nav=segue.destinationViewController;
     ViewController *vc=[nav childViewControllers].firstObject;
    vc.state = switchstate;
    
}

#pragma mark custom fuction

-(NSNumber *)fetch{
    
    AppDelegate *app=[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[app managedObjectContext];
    NSFetchRequest *req=[[NSFetchRequest alloc]initWithEntityName:@"Alarm"];
    
    self.obj=[context executeFetchRequest:req error:nil];
    self.alarmnum1=[self.obj valueForKey:@"alarmnum"];
    NSLog(@"alarmnum inside fuction=%@",self.alarmnum1);
    NSLog(@"alarmnum lastobject inside fuction=%@",self.alarmnum1.lastObject);
    
    return self.alarmnum1.lastObject;
}

- (IBAction)btndone:(id)sender {
    
    //localnotification
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HH:mm"];
    self.pickertime=self.datepicker.date;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:self.pickertime];
    hour = [components hour];
    minute = [components minute];
    
    NSTimeInterval interval;
    interval =[self.pickertime timeIntervalSinceNow];
    
    UILocalNotification *local = [[UILocalNotification alloc]init];
    local.fireDate = [NSDate dateWithTimeIntervalSinceNow:interval];
    local.alertBody = @"Alarm";
    local.timeZone=[NSTimeZone defaultTimeZone];
//    local.repeatInterval = NSCalendarUnitMinute;
    
    [[UIApplication sharedApplication]scheduleLocalNotification:local];
    
    NSLog(@"switch state inside btndone=%d",switchstate);

//   save to coredata
    
    AppDelegate *app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    //AppDelegate *app=[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context=[app managedObjectContext];
    NSManagedObject *new;
    new=[NSEntityDescription insertNewObjectForEntityForName:@"Alarm" inManagedObjectContext:context];
    
    
    if (app.user1==1) {
        
        static int i=0;
        i++;
        NSLog(@"first time alarm num=%ld",(long)i);
        NSNumber *alarmnum=[NSNumber numberWithInteger:i];
        [new setValue:alarmnum forKey:@"alarmnum"];
        
    }
    else {
        
       //NSInteger i=[[self fetch] integerValue];
       NSInteger i=[self.num integerValue];
       i++;
       NSLog(@"alarmnum inside button=%ld",(long)i);
        
       NSNumber *alarmnum=[NSNumber numberWithInteger:i];
       [new setValue:alarmnum forKey:@"alarmnum"];

        
    }
    
    
    NSNumber *hour1=[NSNumber numberWithInteger:hour];
    [new setValue:hour1 forKey:@"alarmtimehour"];
    
    NSNumber *minute1=[NSNumber numberWithInteger:minute];
    [new setValue:minute1 forKey:@"alarmtimemin"];
    
     NSNumber *switchstate1=[NSNumber numberWithBool:switchstate];
    [new setValue:switchstate1 forKey:@"switchstate"];
    
    NSError *error;
    [context save:&error];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
   }


- (IBAction)sw:(id)sender {
    
    if (self.sw.on)
        
        switchstate =@"on";
    else
        
        switchstate=@"off";
}
@end
