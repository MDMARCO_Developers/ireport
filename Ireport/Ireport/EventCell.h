//
//  EventCell.h
//  Ireport
//
//  Created by MDMARCO on 18/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *eventDate;
@property (strong, nonatomic) IBOutlet UILabel *eventMAtter;
@property (strong, nonatomic) IBOutlet UIView *eventColor;

@end
