//
//  studyFileView.h
//  Ireport
//
//  Created by MDMARCO on 07/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface studyFileView : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableview;


@end
