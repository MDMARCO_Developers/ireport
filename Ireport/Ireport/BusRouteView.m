//
//  BusRouteView.m
//  Ireport
//
//  Created by MDMARCO on 08/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "BusRouteView.h"
#import "busrouteCell.h"

@interface BusRouteView ()
{
    NSDictionary *dicBus;
    NSArray *from,*to,*busid,*fromlat,*fromlong,*tolat,*tolong;
}

@end

@implementation BusRouteView

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadBus];
}

-(void)loadBus
{
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/BusList.php?Scode=1001"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  dicBus=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  busid=[dicBus valueForKey:@"id"];
                                  from=[dicBus valueForKey:@"fromplace"];
                                  to=[dicBus  valueForKey:@"toplace"];
                                  
                                  
                                  fromlat=[dicBus valueForKey:@"fromlatitude"];
                                  tolat=[dicBus  valueForKey:@"tolatitude"];
                                  fromlong=[dicBus valueForKey:@"fromlongitude"];
                                  tolong=[dicBus  valueForKey:@"tolongitude"];
                                  
                                  
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      
                                      NSLog(@"location = %@",dicBus);                 [self.busTable reloadData];
                                  });
                              }];
    
    [dt resume];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return busid.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    busrouteCell *cell=[tableView dequeueReusableCellWithIdentifier:@"bus" forIndexPath:indexPath];
    cell.bus_route.text=[NSString stringWithFormat:@"%@ - %@",[from objectAtIndex:indexPath.section],[to objectAtIndex:indexPath.section]];
    [cell.bus_number setTitle:[NSString stringWithFormat:@"No.%ld",indexPath.section+1] forState:UIControlStateNormal];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults]setObject:[fromlat objectAtIndex:indexPath.section] forKey:@"fromlat"];
    [[NSUserDefaults standardUserDefaults]setObject:[tolat objectAtIndex:indexPath.section] forKey:@"tolat"];
    [[NSUserDefaults standardUserDefaults]setObject:[fromlong objectAtIndex:indexPath.section] forKey:@"fromlongt"];
    [[NSUserDefaults standardUserDefaults]setObject:[tolong objectAtIndex:indexPath.section] forKey:@"tolong"];
    [self performSegueWithIdentifier:@"buslocation" sender:self];
}

@end
