//
//  newsView.h
//  Ireport
//
//  Created by MDMARCO on 13/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface newsView : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *newsTable;
@property(strong,nonatomic)NSDictionary *newsDictionary;
@property(strong,nonatomic)NSArray *newsHead,*newsContent,*newsTime,*newsDate,*newsImage;
-(void)getNews;

@end
