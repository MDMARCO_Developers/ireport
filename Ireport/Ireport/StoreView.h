//
//  StoreView.h
//  Ireport
//
//  Created by MDMARCO on 14/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreView : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet UICollectionView *StoreCollection;
@property (strong,nonatomic) NSArray *image,*name,*price,*pid;
@property(strong,nonatomic)NSDictionary *productDic;
-(void)getProductDetails;
@end
