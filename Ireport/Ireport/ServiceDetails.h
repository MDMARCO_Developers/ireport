//
//  ServiceDetails.h
//  Ireport
//
//  Created by MDMARCO on 13/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceDetails : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *serviceContact;
@property (strong, nonatomic) IBOutlet UITextView *servicedescription;
@property (strong, nonatomic) IBOutlet UIImageView *serviceImage;
- (IBAction)btnCall:(id)sender;

@end
