//
//  GraphDetails.m
//  Ireport
//
//  Created by MDMARCO on 04/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "GraphDetails.h"
#import "ExamListTableCell.h"

@interface GraphDetails ()
{
    NSDictionary *markDic;
    NSArray *notdate,*examdate,*examName,*subName,*marks,*total,*exId,*examGrade;
    NSString *strid;
    CGFloat height,width;
}

@end

@implementation GraphDetails

- (void)viewDidLoad
{
    [super viewDidLoad];
    width=self.graphView.frame.size.width;
    height=self.graphView.frame.size.height;
    strid=[[NSUserDefaults standardUserDefaults]stringForKey:@"lang_id"];
    NSLog(@"lang id = %@",strid);
    [self getMarks];
  
}
-(void)getMarks
{
    //http://192.168.0.108/school/appdata/Graph_subject_mark_list.php?Scode=1001&class_id=1&subject_id=817626687&registerNo=1001
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/Graph_subject_mark_list.php?Scode=1001&class_id=1&subject_id=%@&registerNo=1001",strid]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  markDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  exId=[markDic valueForKey:@"id"];
                                  notdate=[markDic valueForKey:@"Date"];
                                  examdate=[markDic  valueForKey:@"ExamDate"];
                                  examName=[markDic valueForKey:@"ExamName"];
                                  subName=[markDic  valueForKey:@"SubjectName"];
                                  examGrade=[markDic  valueForKey:@"grade"];
                                  marks=[markDic valueForKey:@"Marks"];
                                  total=[markDic valueForKey:@"TotalMarks"];
                                  
                                  
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      self.subjectName.text=[NSString stringWithFormat:@"%@",[subName objectAtIndex:0]];

                                      [self loadGraph];
                                      [self.chart reloadData];
                                      
                                      [self.marklist reloadData];
                                  });
                              }];
    
    [dt resume];
    
}
-(void)loadGraph
{
    CGRect chartFrame				= CGRectMake(0.0,
                                                 0.0,
                                                 width,
                                                 height);
    self.chart							= [[SimpleBarChart alloc] initWithFrame:chartFrame];
    self.chart.center					= CGPointMake(self.graphView.frame.size.width / 2.0, self.graphView.frame.size.height / 2.0);
    self.chart.delegate					= self;
    self.chart.dataSource				= self;
    self.chart.barShadowOffset			= CGSizeMake(2.0, 1.0);
    self.chart.animationDuration		= 1.0;
    self.chart.barShadowColor			= [UIColor grayColor];
    self.chart.barShadowAlpha			= 0.5;
    self.chart.barShadowRadius			= 1.0;
    self.chart.barWidth                 = 35.0;
    self.chart.barTextType =SimpleBarChartXLabelTypeVerticle;
    self.chart.xLabelType				= SimpleBarChartXLabelTypeVerticle;
    self.chart.incrementValue			= 10;
    
    self.chart.barTextColor				= [UIColor redColor];
    self.chart.gridColor				= [UIColor blackColor];
    self.barColors = @[[UIColor blueColor], [UIColor redColor], [UIColor blackColor], [UIColor orangeColor], [UIColor purpleColor], [UIColor greenColor]];
    
    [self.graphView addSubview:self.chart];

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return exId.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ExamListTableCell *cell =[tableView dequeueReusableCellWithIdentifier:@"mlist" forIndexPath:indexPath];
    cell.exDate.text=[NSString stringWithFormat:@"%@",[examdate objectAtIndex:indexPath.section]];
    cell.exName.text=[NSString stringWithFormat:@"%@",[examName objectAtIndex:indexPath.section]];
    cell.exGrade.text=[NSString stringWithFormat:@"%@",[examGrade objectAtIndex:indexPath.section]];
    cell.markobt.text=[NSString stringWithFormat:@"%@ / %@",[marks objectAtIndex:indexPath.section],[total  objectAtIndex:indexPath.section]];
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}



//graph delegates

#pragma mark SimpleBarChartDataSource

- (NSUInteger)numberOfBarsInBarChart:(SimpleBarChart *)barChart
{
    return examName.count;
}

- (CGFloat)barChart:(SimpleBarChart *)barChart valueForBarAtIndex:(NSUInteger)index
{
    return [[marks objectAtIndex:index] floatValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart textForBarAtIndex:(NSUInteger)index
{
    return [NSString stringWithFormat:@"%@/%@",[marks objectAtIndex:index],[total objectAtIndex:index]];
}

- (NSString *)barChart:(SimpleBarChart *)barChart xLabelForBarAtIndex:(NSUInteger)index
{
    return [examName objectAtIndex:index] ;
}

- (UIColor *)barChart:(SimpleBarChart *)barChart colorForBarAtIndex:(NSUInteger)index
{
    return [self.barColors objectAtIndex:index];
}



@end
