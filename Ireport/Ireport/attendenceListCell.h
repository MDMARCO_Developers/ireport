//
//  attendenceListCell.h
//  Ireport
//
//  Created by MDMARCO on 14/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface attendenceListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *absentDate;
@property (strong, nonatomic) IBOutlet UILabel *informDate;

@end
