//
//  reportView.m
//  Ireport
//
//  Created by MDMARCO on 04/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "reportView.h"
#import "reportTableViewCell.h"
@interface reportView ()
{
    NSMutableArray *suball,*subId;
    NSString *str1;
}

@end

@implementation reportView

- (void)viewDidLoad
{
    [super viewDidLoad];
    str1=[[NSUserDefaults standardUserDefaults]stringForKey:@"abcd"];
    suball=[[NSMutableArray alloc]init];
    subId=[[NSMutableArray alloc]init];
    [self subjects];
   
}
-(void)subjects
{
    //http://192.168.0.108/school/appdata/Student_Subject_list.php?Scode=1001&class_id=1&language_id=817626687
    
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/Student_Subject_list.php?Scode=1001&class_id=1&language_id=817626687"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                    self.subjectDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  self.subject_Id=[self.subjectDic valueForKey:@"subject_id"];
                                     self.subject_Name=[self.subjectDic  valueForKey:@"subject"];
                                  self.sublang_Id=[self.subjectDic valueForKey:@"sublanguage_id"];
                                   self.sublang_Name=[self.subjectDic  valueForKey:@"sublanguage"];
                                  
                                  
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      
                                      [suball addObjectsFromArray:self.subject_Name];
                                      [suball addObject:[self.sublang_Name objectAtIndex:0]];
                                      [subId addObjectsFromArray:self.subject_Id];
                                      [subId addObject:[self.sublang_Id objectAtIndex:0]];
                                      
                                      [self.subject_Table reloadData];
                                      
                                  });
                              }];
    [dt resume];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return suball.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    reportTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"report" forIndexPath:indexPath];
    cell.subjectName.text=[NSString stringWithFormat:@"%@",[suball objectAtIndex:indexPath.section]];
    NSLog(@"count = %lu",indexPath.section);
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults]setObject:[subId objectAtIndex:indexPath.section] forKey:@"lang_id"];
    NSLog(@"subject = %@ id =%@",[suball objectAtIndex:indexPath.section],[subId objectAtIndex:indexPath.section]);
    if([str1 isEqualToString:@"report"])
    {
    [self performSegueWithIdentifier:@"graphDetails" sender:self];
    }
    else
    {
        [self performSegueWithIdentifier:@"studyfile" sender:self];
    }
}



@end
