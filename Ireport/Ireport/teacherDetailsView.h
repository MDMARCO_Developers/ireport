//
//  teacherDetailsView.h
//  Ireport
//
//  Created by MDMARCO on 13/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface teacherDetailsView : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *teacherImg;
@property (strong, nonatomic) IBOutlet UILabel *teacherName;
@property (strong, nonatomic) IBOutlet UILabel *teacherSubject;
@property (strong, nonatomic) IBOutlet UILabel *teacherNumber;
- (IBAction)callButton:(id)sender;
@property(strong,nonatomic)NSString *name,*subject,*phone,*img;

@end
