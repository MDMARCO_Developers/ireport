//
//  Attendenceview.m
//  Ireport
//
//  Created by MDMARCO on 14/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "Attendenceview.h"
#import "attendenceListCell.h"

@interface Attendenceview ()

@end

@implementation Attendenceview

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getAttendence];
    
}
-(void)getAttendence
{
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/attendance.php?Scode=1001&registerNo=1001"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  
                                  self.attendenceDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  self.absentDate=[self.attendenceDic valueForKey:@"AbsentDate"];
                                  
                                  self.notifyDate=[self.attendenceDic  valueForKey:@"date"];
                                  
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      
                                      [self.AbsentTable reloadData];
                                      
                                  });
                              }];
    
    [dt resume];
    

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.absentDate.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    attendenceListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"attendance"     forIndexPath:indexPath];
    cell.absentDate.text=[NSString stringWithFormat:@"%@",[self.absentDate objectAtIndex:indexPath.section]];
    cell.informDate.text=[NSString stringWithFormat:@"%@",[self.notifyDate objectAtIndex:indexPath.section]];
    cell.backgroundColor =[UIColor whiteColor];
    //cell.layer.borderColor = [UIColor blackColor];
    cell.layer.borderWidth = 1;
    cell.layer.cornerRadius = 8;
    cell.clipsToBounds = true;
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
