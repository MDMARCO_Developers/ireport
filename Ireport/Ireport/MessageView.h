//
//  MessageView.h
//  Ireport
//
//  Created by MDMARCO on 19/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageView : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *message_table;
@property (strong, nonatomic) IBOutlet UIView *views;
@property (strong, nonatomic) IBOutlet UITextField *msgtxt;
@end
