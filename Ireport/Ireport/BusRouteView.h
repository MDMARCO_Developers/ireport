//
//  BusRouteView.h
//  Ireport
//
//  Created by MDMARCO on 08/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusRouteView : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *busTable;

@end
