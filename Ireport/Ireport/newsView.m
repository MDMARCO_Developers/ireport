//
//  newsView.m
//  Ireport
//
//  Created by MDMARCO on 13/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "newsView.h"
#import "UIImageView+WebCache.h"
#import "newsViewCell.h"

@interface newsView ()
{
    CGFloat width,height;
}

@end

@implementation newsView

- (void)viewDidLoad
{
    [super viewDidLoad];
    width=[[UIScreen mainScreen] bounds].size.width;
    [self getNews];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)getNews
{
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/news.php?Scode=1001"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  
                                  self.newsDictionary=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  self.newsHead=[self.newsDictionary valueForKey:@"news_headline"];
                                  
                                  self.newsContent=[self.newsDictionary  valueForKey:@"news"];
                                  self.newsDate=[self.newsDictionary  valueForKey:@"date"];
                                  self.newsTime=[self.newsDictionary  valueForKey:@"time"];
                                                                    self.newsImage=[self.newsDictionary  valueForKey:@"news_image1"];
                                  //                                  self.teacherId=[self.serviceDictionary  valueForKey:@"teacherid"];
                                  
                                  //NSLog(@"%@",self.categoryDic);
                                  //for storing mutable array
                                  //=====================
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      
                                      [self.newsTable reloadData];
                                      
                                  });
                              }];
    
    [dt resume];

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.newsHead.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    newsViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"news" forIndexPath:indexPath];
    cell.newsHeading.text=[NSString stringWithFormat:@"%@",[self.newsHead objectAtIndex:indexPath.row]];
    cell.newsHeading.numberOfLines=0;
    [cell.newsHeading setLineBreakMode:NSLineBreakByWordWrapping];
    cell.newsDate.text=[NSString stringWithFormat:@"%@",[self.newsDate objectAtIndex:indexPath.row]];
    cell.newsTime.text=[NSString stringWithFormat:@"%@",[self.newsTime objectAtIndex:indexPath.row]];
    cell.newsDescription.text=[NSString stringWithFormat:@"%@",[self.newsContent objectAtIndex:indexPath.row]];
    
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/DIARY/school/news_images/%@",[self.newsImage objectAtIndex:indexPath.row]]];
    [[cell newsImage]sd_setImageWithURL:imageUrl];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (3*width);
}

@end
