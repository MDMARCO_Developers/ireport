//
//  newsViewCell.h
//  Ireport
//
//  Created by MDMARCO on 13/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface newsViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *newsHeading;
@property (strong, nonatomic) IBOutlet UILabel *newsDate;
@property (strong, nonatomic) IBOutlet UIImageView *newsImage;
@property (strong, nonatomic) IBOutlet UITextView *newsDescription;
@property (strong, nonatomic) IBOutlet UILabel *newsTime;

@end
