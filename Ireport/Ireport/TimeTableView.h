//
//  TimeTableView.h
//  Ireport
//
//  Created by MDMARCO on 10/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeTableView : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *time_table;
@property (strong, nonatomic) IBOutlet UILabel *weakday;
@property (strong, nonatomic) IBOutlet UIPageControl *pageindex;

@end
