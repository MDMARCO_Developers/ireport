//
//  ServiceDetails.m
//  Ireport
//
//  Created by MDMARCO on 13/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "ServiceDetails.h"
#import "UIImageView+WebCache.h"

@interface ServiceDetails ()
{
    NSString *img;
}

@end

@implementation ServiceDetails

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.serviceContact.text=[[NSUserDefaults standardUserDefaults]stringForKey:@"scontact"];
    self.servicedescription.text=[[NSUserDefaults standardUserDefaults]stringForKey:@"sdescription"];
     img=[[NSUserDefaults standardUserDefaults]stringForKey:@"spic"];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/DIARY/school/service_images/%@",img]];
    [self.serviceImage sd_setImageWithURL:imageUrl];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

- (IBAction)btnCall:(id)sender
{
    
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.serviceContact.text]]];
}
@end
