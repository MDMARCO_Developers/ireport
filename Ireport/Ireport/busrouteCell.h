//
//  busrouteCell.h
//  Ireport
//
//  Created by MDMARCO on 08/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface busrouteCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *bus_route;
@property (strong, nonatomic) IBOutlet UIButton *bus_number;

@end
