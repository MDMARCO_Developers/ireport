//
//  teachersListCell.h
//  Ireport
//
//  Created by MDMARCO on 09/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface teachersListCell : UITableViewCell
@property (strong,nonatomic)IBOutlet UIImageView *teacherImage;
@property (strong,nonatomic)IBOutlet UILabel *teacherName;
@property (strong,nonatomic)IBOutlet UILabel *teacherSubject;
@property (strong, nonatomic) IBOutlet UIButton *btnmessage;
@end
