//
//  GradeBehaviourView.h
//  Ireport
//
//  Created by MDMARCO on 07/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNPieChart.h"
#import "PNChartDelegate.h"

@interface GradeBehaviourView : UIViewController
@property (strong, nonatomic) IBOutlet UIView *gradeView;
@property (strong, nonatomic) IBOutlet UIImageView *gradeImage;

@property (strong, nonatomic) IBOutlet UIScrollView *details;

@property (strong, nonatomic) IBOutlet UITextView *ContentS;

@property (nonatomic) PNPieChart *pieChart;
@property (strong,nonatomic) NSDictionary *gradeDic;
@property (strong,nonatomic) NSArray *behaviour,*titlehead,*mtr,*img,*dates;
@property (strong, nonatomic) IBOutlet UIButton *goodbtn;
@property (strong, nonatomic) IBOutlet UIButton *badbtn;
@end
