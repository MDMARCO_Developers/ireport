//
//  StoreView.m
//  Ireport
//
//  Created by MDMARCO on 14/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "StoreView.h"
#import "StoreViewCell.h"
#import "UIImageView+WebCache.h"
@interface StoreView ()
{
    CGFloat width;
}

@end

@implementation StoreView

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     width = [UIScreen mainScreen].bounds.size.width;
    [self getProductDetails];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)getProductDetails
{
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/store_product.php?Scode=1001"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  
                                  self.productDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  self.pid=[self.productDic valueForKey:@"product_id"];
                                  
                                  self.name=[self.productDic  valueForKey:@"product_name"];
                                  self.image=[self.productDic  valueForKey:@"product_image"];
                                  self.price=[self.productDic  valueForKey:@"price"];
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      
                                      [self.StoreCollection reloadData];
                                      
                                  });
                              }];
    
    [dt resume];

}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.pid.count;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    // int width1=CGRectGetWidth(collectionView.frame);
    return CGSizeMake((width/2)-13,(width/2));
    
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StoreViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"store" forIndexPath:indexPath];
    cell.itemName.text=[NSString stringWithFormat:@"%@",[self.name objectAtIndex:indexPath.item]];
    cell.itemprice.text=[NSString stringWithFormat:@"%@",[self.price objectAtIndex:indexPath.item]];
    NSURL *myurl=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/DIARY/school/storeimage/%@",[self.image objectAtIndex:indexPath.item]]];
    [[cell itemImage]sd_setImageWithURL:myurl ];
    return cell;
}
@end
