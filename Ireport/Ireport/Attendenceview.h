//
//  Attendenceview.h
//  Ireport
//
//  Created by MDMARCO on 14/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Attendenceview : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic)NSDictionary *attendenceDic;
@property(strong,nonatomic)NSArray *absentDate,*notifyDate;
@property (strong, nonatomic) IBOutlet UITableView *AbsentTable;
-(void)getAttendence;


@end
