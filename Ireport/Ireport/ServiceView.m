//
//  ServiceView.m
//  Ireport
//
//  Created by MDMARCO on 13/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "ServiceView.h"
#import "serviceListCell.h"
#import "UIImageView+WebCache.h"
@interface ServiceView ()

@end

@implementation ServiceView

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getService];
}
-(void)getService
{
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/services.php?Scode=1001"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  
                                  self.serviceDictionary=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  self.sName=[self.serviceDictionary valueForKey:@"service_name"];
                                  
                                  self.sContact=[self.serviceDictionary  valueForKey:@"contact"];
                                  self.sDescription=[self.serviceDictionary  valueForKey:@"service_description"];
                                  self.sImage=[self.serviceDictionary  valueForKey:@"service_image"];
//                                  self.s=[self.serviceDictionary  valueForKey:@"status"];
//                                  self.teacherId=[self.serviceDictionary  valueForKey:@"teacherid"];
                                  
                                  //NSLog(@"%@",self.categoryDic);
                                  //for storing mutable array
                                  //=====================
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      
                                      [self.tableService reloadData];
                                      
                                  });
                              }];
    
    [dt resume];

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sName.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    serviceListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"service" forIndexPath:indexPath];
    cell.serviceName.text=[NSString stringWithFormat:@"%@",[self.sName objectAtIndex:indexPath.section]];
   
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/DIARY/school/service_images/%@",[self.sImage objectAtIndex:indexPath.section]]];
    [[cell serviceImage]sd_setImageWithURL:imageUrl];
    
    cell.serviceImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
    cell.serviceImage.layer.cornerRadius=cell.serviceImage.frame.size.height/2;
    cell.serviceImage.layer.borderWidth=2.0;
    cell.serviceImage.layer.masksToBounds = YES;
    cell.serviceImage.layer.borderColor=[[UIColor redColor] CGColor];
    cell.backgroundColor =[UIColor whiteColor];
    //cell.layer.borderColor = [UIColor blackColor];
    cell.layer.borderWidth = 1;
    cell.layer.cornerRadius = 8;
    cell.clipsToBounds = true;
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults]setObject:[self.sName objectAtIndex:indexPath.section] forKey:@"sname"];
    [[NSUserDefaults standardUserDefaults]setObject:[self.sImage objectAtIndex:indexPath.section] forKey:@"spic"];
    [[NSUserDefaults standardUserDefaults]setObject:[self.sDescription objectAtIndex:indexPath.section] forKey:@"sdescription"];
    [[NSUserDefaults standardUserDefaults] setObject:[self.sContact objectAtIndex:indexPath.section] forKey:@"scontact"];
    [self performSegueWithIdentifier:@"serviceDetails" sender:self];
    
}


- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
}
@end
