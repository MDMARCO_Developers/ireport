//
//  AboutUsView.h
//  Ireport
//
//  Created by MDMARCO on 14/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface AboutUsView : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *SchoolImage;
@property (strong, nonatomic) IBOutlet UITextView *SchoolDetails;
@property (strong, nonatomic) IBOutlet MKMapView *SchoolMap;
-(void)GetAboutUsDetails;
@property (strong,nonatomic)NSDictionary *aboutDic;
@property (strong,nonatomic)NSArray *simage,*address,*phone1,*phone2,*phone3,*sdescription,*slat,*slong;
@end
