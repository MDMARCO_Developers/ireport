//
//  teacherlist.m
//  Ireport
//
//  Created by MDMARCO on 09/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "teacherlist.h"
#import "teachersListCell.h"
#import "UIImageView+WebCache.h"

@interface teacherlist ()

@end

@implementation teacherlist

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getTeacher_Details];
}
-(void)getTeacher_Details
{

    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/teachers_list.php?Scode=1001&Class_id=1&Division=A"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  
                                  self.teacherDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  self.teacherName=[self.teacherDic valueForKey:@"teachername"];
                                  
                                  self.teacherSubject=[self.teacherDic  valueForKey:@"subject"];
                                  self.teacherPic=[self.teacherDic  valueForKey:@"photo"];
                                  self.teacherphone=[self.teacherDic  valueForKey:@"phonenumber"];
                                    self.status=[self.teacherDic  valueForKey:@"status"];
                                    self.teacherId=[self.teacherDic  valueForKey:@"teacherid"];
                                  
                                  //NSLog(@"%@",self.categoryDic);
                                  //for storing mutable array
                                                                    //=====================
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      
                                      [self.teacherTable reloadData];
                                      
                                  });
                              }];
    
    [dt resume];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.teacherName.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    teachersListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"teacher" forIndexPath:indexPath];
    cell.teacherName.text=[NSString stringWithFormat:@"%@",[self.teacherName objectAtIndex:indexPath.section]];
    cell.teacherSubject.text=[NSString stringWithFormat:@"%@",[self.teacherSubject objectAtIndex:indexPath.section]];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/DIARY/school/teacherimage/%@",[self.teacherPic objectAtIndex:indexPath.section]]];
    [[cell teacherImage]sd_setImageWithURL:imageUrl];
    
    cell.teacherImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
    cell.teacherImage.layer.cornerRadius=cell.teacherImage.frame.size.height/2;
    cell.teacherImage.layer.borderWidth=2.0;
    cell.teacherImage.layer.masksToBounds = YES;
   cell.teacherImage.layer.borderColor=[[UIColor redColor] CGColor];
    cell.backgroundColor =[UIColor whiteColor];
    [cell.btnmessage setTag:indexPath.section];
    

    //cell.layer.borderColor = [UIColor blackColor];
    cell.layer.borderWidth = 1;
    cell.layer.cornerRadius = 8;
    cell.clipsToBounds = true;
   // NSLog(@" message tag = %@d",)
    return cell;
    
}
- (IBAction)buttonclicked:(UIButton *)sender
{
    [[NSUserDefaults standardUserDefaults]setObject:[_teacherName objectAtIndex:sender.tag] forKey:@"teacher_name"];
    [[NSUserDefaults standardUserDefaults]setObject:[self.teacherId objectAtIndex:sender.tag] forKey:@"teacher_id"];
    [[NSUserDefaults standardUserDefaults]setObject:[self.teacherSubject objectAtIndex:sender.tag] forKey:@"subject"];
    NSLog(@"teacher name %@ tag=%ld",[self.teacherName objectAtIndex:sender.tag],(long)sender.tag);
    [self performSegueWithIdentifier:@"messages" sender:self];
}


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 45.0;
//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults]setObject:[self.teacherName objectAtIndex:indexPath.section] forKey:@"name"];
    [[NSUserDefaults standardUserDefaults]setObject:[self.teacherPic objectAtIndex:indexPath.section] forKey:@"pic"];
    [[NSUserDefaults standardUserDefaults]setObject:[self.teacherSubject objectAtIndex:indexPath.section] forKey:@"subject"];
    [[NSUserDefaults standardUserDefaults] setObject:[self.teacherphone objectAtIndex:indexPath.section] forKey:@"teacherMobile"];
    
    [self performSegueWithIdentifier:@"teacherDetails" sender:self];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}

@end
