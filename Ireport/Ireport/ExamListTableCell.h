//
//  ExamListTableCell.h
//  Ireport
//
//  Created by MDMARCO on 04/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExamListTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *exName;
@property (strong, nonatomic) IBOutlet UILabel *exGrade;
@property (strong, nonatomic) IBOutlet UILabel *markobt;

@property (strong, nonatomic) IBOutlet UILabel *exDate;
@end
