//
//  GradeBehaviourView.m
//  Ireport
//
//  Created by MDMARCO on 07/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "GradeBehaviourView.h"
#import "UIImageView+WebCache.h"

@interface GradeBehaviourView ()
{
    CGFloat width,height;
}


@end

@implementation GradeBehaviourView

- (void)viewDidLoad
{
    [super viewDidLoad];
    width=self.gradeView.frame.size.width;
    height=self.gradeView.frame.size.height;
    [self loadDetails];
    
}
-(void)loadDetails
{
    //
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/GradeBehaviour.php?register_no=1001&code=1001"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  self.gradeDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  self.behaviour=[self.gradeDic valueForKey:@"behaviour_point"];
                                  self.mtr=[self.gradeDic  valueForKey:@"matter"];
                                  self.titlehead=[self.gradeDic valueForKey:@"title"];
                                  self.img=[self.gradeDic valueForKey:@"image"];
                                  self.dates=[self.gradeDic valueForKey:@"date"];
                                  
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      NSURL *url1=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/DIARY/school/grade_behaviour/%@",[self.img objectAtIndex:0]]];
                                      [self.gradeImage sd_setImageWithURL:url1];
                                      self.ContentS.text=[NSString stringWithFormat:@"%@",[self.mtr objectAtIndex:0]];
                                      [self gradeviewShow];
                                      
                                     
                                  });
                              }];
    
    [dt resume];
    

}

-(void)gradeviewShow
{
    // description:@"GOOD",description:@"Care"
    NSArray *items = @[                       [PNPieChartDataItem dataItemWithValue:[[self.behaviour objectAtIndex:0] integerValue] color:[UIColor blueColor]],
                                              [PNPieChartDataItem dataItemWithValue:100-[[self.behaviour objectAtIndex:0] integerValue] color:[UIColor redColor] ],
                       ];
    self.goodbtn.backgroundColor=[UIColor blueColor];
    self.badbtn.backgroundColor=[UIColor redColor];
    
    self.pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake((CGFloat) 10,10,width-35,height-35) items:items];
    self.pieChart.descriptionTextColor = [UIColor whiteColor];
    self.pieChart.descriptionTextFont = [UIFont fontWithName:@"Avenir-Medium" size:11.0];
    self.pieChart.descriptionTextShadowColor = [UIColor clearColor];
    self.pieChart.showAbsoluteValues = NO;
    self.pieChart.showOnlyValues = NO;
    [self.pieChart strokeChart];
    self.pieChart.center=CGPointMake(width/2, height/2);
    
    
//    self.pieChart.legendStyle = PNLegendItemStyleStacked;
//    self.pieChart.legendFont = [UIFont boldSystemFontOfSize:12.0f];
    
       [self.gradeView addSubview:self.pieChart];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
}

@end
