//
//  ServiceView.h
//  Ireport
//
//  Created by MDMARCO on 13/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceView : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableService;
@property(strong,nonatomic)NSDictionary *serviceDictionary;
@property(strong,nonatomic)NSArray *sName,*sDescription,*sContact,*sImage;
-(void)getService;
@end
