//
//  MessageView.m
//  Ireport
//
//  Created by MDMARCO on 19/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "MessageView.h"
#import "messageCell.h"
@import FirebaseDatabase;
@import Firebase;
@import FirebaseAuth;
NSString *userID;

@interface MessageView ()

{
    NSString *teacherName,*teacherId,*teacherSub;
}
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) IBOutlet UIButton *btnsend;

@end

@implementation MessageView

- (void)viewDidLoad
{
    [super viewDidLoad];
    //userID = [FIRAuth auth].currentUser.uid;
    self.ref = [[FIRDatabase database] reference];
    teacherId=[[NSUserDefaults standardUserDefaults]stringForKey:@"teacher_id"];
    teacherName=[[NSUserDefaults standardUserDefaults]stringForKey:@"teacher_name"];
    teacherSub=[[NSUserDefaults standardUserDefaults]stringForKey:@"subject"];
    
    NSLog(@"child reference = %@ ",self.ref);
    CGFloat fl=self.views.frame.size.height;
    NSLog(@"height of view = %f",fl);
    //self.btnsend.enabled=NO;
   
}




-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    messageCell *cell=[tableView dequeueReusableCellWithIdentifier:@"message" forIndexPath:indexPath];
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)keyboardWillShow:(NSNotification*)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat flt=self.message_table.frame.size.height;
    CGRect rect = self.view.frame;
    rect.size.height -= keyboardSize.height;

    NSLog(@"CGRECT = %f table =%f",keyboardSize.height,flt);
}
- (IBAction)btnSend:(id)sender
{
    if([_msgtxt.text isEqualToString:@""])
    {
        NSLog(@"cannot send without null value");
    }
    else
    {
        NSDictionary *feedItems = @{
                                    @"messge" : self.msgtxt.text,@"type":@"sender",
                                    };
        [[[_ref child:@"10011001"]child:[NSString stringWithFormat:@"%@%@",teacherName,teacherId]] updateChildValues:feedItems ];
      //  [[[_ref child:@"10011001"] child:[NSString stringWithFormat:@"%@%@",teacherName,teacherId]] setValue:feedItems];
//        [[[[_ref child:@"10011001"] child:[NSString stringWithFormat:@"%@%@",teacherName,teacherId]] child:@"type"] setValue:@"sender"];
    

      //  NSDictionary *childupdates=@{@"messge":_msgtxt.text, @"type":@"sender"};
     //   NSDictionary *childUpdates = @{[@"/10011001/" stringByAppendingString:key]: post,[NSString stringWithFormat:@"/%@/"%@%@,key]: post};
       // [_ref updateChildValues:childupdates];
    
    }
}

@end
