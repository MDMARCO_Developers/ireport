//
//  careerView.h
//  Ireport
//
//  Created by MDMARCO on 08/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface careerView : UIViewController<UIGestureRecognizerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
- (IBAction)btnNext:(id)sender;

@end
