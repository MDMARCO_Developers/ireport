//
//  LooginView.h
//  Ireport
//
//  Created by MDMARCO on 07/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LooginView : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *SCode;

@property (strong, nonatomic) IBOutlet UITextField *Reg_Number;
@property (strong, nonatomic) IBOutlet UITextField *Security_Key;
- (IBAction)btnLogin:(id)sender;
@end
