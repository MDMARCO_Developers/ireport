//
//  GraphDetails.h
//  Ireport
//
//  Created by MDMARCO on 04/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleBarChart.h"
@interface GraphDetails : UIViewController<UITableViewDelegate,UITableViewDataSource,SimpleBarChartDataSource, SimpleBarChartDelegate>


@property (strong, nonatomic) IBOutlet UIView *graphView;

@property (strong, nonatomic) IBOutlet UITableView *marklist;
@property (strong, nonatomic) IBOutlet UILabel *subjectName;

@property (strong,nonatomic) SimpleBarChart *chart;
@property (strong,nonatomic)NSArray *barColors;
@end
