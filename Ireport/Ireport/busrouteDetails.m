//
//  busrouteDetails.m
//  Ireport
//
//  Created by MDMARCO on 18/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "busrouteDetails.h"
@import GoogleMaps;

@interface busrouteDetails ()
{
    NSString *fromlat,*tolat,*fromlong,*tolong;
}

@end

@implementation busrouteDetails


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    fromlat=[[NSUserDefaults standardUserDefaults]stringForKey:@"fromlat"];
    tolat=[[NSUserDefaults standardUserDefaults]stringForKey:@"tolat"];
    fromlong=[[NSUserDefaults standardUserDefaults]stringForKey:@"fromlongt"];
    tolong=[[NSUserDefaults standardUserDefaults]stringForKey:@"tolong"];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[fromlat doubleValue]
                                                            longitude:[fromlong doubleValue]
                                                                 zoom:13];
    GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = camera.target;
    marker.snippet = @"i am here";
    marker.appearAnimation = kGMSMarkerAnimationPop;
    
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"style" withExtension:@"json"];
    NSError *error;
    
    // Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    mapView.mapStyle = style;
    marker.map = mapView;
    
    
    
//  Marker.position=CLLocationCoordinate2DMake([fromlat doubleValue], [fromlong doubleValue]);
//  marker.icon=[UIImage imageNamed:@"aaa.png"] ;
    
//    marker.groundAnchor=CGPointMake(0.5,0.5);
//    marker.map=mapView;
//    
//  GMSMutablePath *path = [GMSMutablePath path];
//  [path addCoordinate:CLLocationCoordinate2DMake(fromlat.doubleValue,fromlong.doubleValue)];
//  [path addCoordinate:CLLocationCoordinate2DMake(tolat.doubleValue,tolong.doubleValue)];
//    
//  GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
//  rectangle.strokeWidth = 2.f;
//  rectangle.map = mapView;
    self.view=mapView;
    
    
    
//    NSString *googleMapUrlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", [fromlat doubleValue], [fromlong doubleValue], [tolat doubleValue], [tolong doubleValue]];
//    NSLog(@"google map api = %@",googleMapUrlString);
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapUrlString]];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}



@end
