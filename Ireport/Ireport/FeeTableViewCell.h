//
//  FeeTableViewCell.h
//  Ireport
//
//  Created by MDMARCO on 04/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeeTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titles;
@property (strong, nonatomic) IBOutlet UILabel *feesamount;

@end
