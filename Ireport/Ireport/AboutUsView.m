//
//  AboutUsView.m
//  Ireport
//
//  Created by MDMARCO on 14/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "AboutUsView.h"
#import "UIImageView+WebCache.h"

@interface AboutUsView ()

@end

@implementation AboutUsView

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self GetAboutUsDetails];
}
-(void)GetAboutUsDetails
{
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/AboutUS.php?Scode=1001"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  
                                  self.aboutDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  self.simage=[self.aboutDic valueForKey:@"school_image"];
                                  
                                  self.address=[self.aboutDic  valueForKey:@"address"];
                                  self.phone1=[self.aboutDic valueForKey:@"phone_number1"];
                                  
                                  self.phone2=[self.aboutDic  valueForKey:@"phone_number2"];
                                  self.phone3=[self.aboutDic valueForKey:@"phone_number3"];
                                  
                                  self.sdescription=[self.aboutDic  valueForKey:@"description"];
                                  self.slat=[self.aboutDic valueForKey:@"latitude"];
                                  
                                  self.slong=[self.aboutDic  valueForKey:@"longitude"];
                                  
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      
                                      NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/DIARY/school/about_us/%@",[self.simage objectAtIndex:0]]];
                                      
                                      [[self SchoolImage] sd_setImageWithURL:url];
                                      self.SchoolDetails.text=[NSString stringWithFormat:@"%@\n%@\n%@\n%@",[_address objectAtIndex:0],[_phone1 objectAtIndex:0],[_phone2 objectAtIndex:0],[_phone3 objectAtIndex:0]];
                                
                                      
                                  });
                              }];
    
    [dt resume];
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}



@end
