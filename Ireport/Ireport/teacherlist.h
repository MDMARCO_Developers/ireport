//
//  teacherlist.h
//  Ireport
//
//  Created by MDMARCO on 09/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface teacherlist : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic)NSDictionary *teacherDic;
@property(strong,nonatomic)NSArray *teacherName,*teacherSubject,*teacherPic,*status,*teacherphone,*teacherId;
@property (strong, nonatomic) IBOutlet UITableView *teacherTable;


@end
