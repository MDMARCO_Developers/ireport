//
//  LooginView.m
//  Ireport
//
//  Created by MDMARCO on 07/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "LooginView.h"

@interface LooginView ()

@end

@implementation LooginView

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
  
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLogin:(id)sender
{
    if([self.SCode.text isEqualToString:@""]&&[self.Reg_Number.text isEqualToString:@""]&&[self.Security_Key.text isEqualToString:@""])
    {
        NSLog(@"Invalid credentials");
    }
    else
    {
        [self datatask1];
    }
}
-(void)datatask1
{ NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/student_login.php?"]];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSString * params =[NSString stringWithFormat:@"Scode=%@&register_number=%@&security_key=%@", self.SCode.text, self.Reg_Number.text,self.Security_Key.text];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest];
    [dataTask setTaskDescription:@"task1"];
    [dataTask resume];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    NSLog(@"### handler 1");
    
    completionHandler(NSURLSessionResponseAllow);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    if([[dataTask taskDescription] isEqualToString:@"task1"])
    {
        NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"Received String %@",str);
        NSArray *ar=[NSArray arrayWithObject:[NSString stringWithFormat:@"%@",str]];
       // NSString *str
        if ([str containsString:@"1"])
        {
          //  NSDictionary *dc=[NSDictionary dictionaryWithObjectsAndKeys:str, nil];
            
            NSLog(@"logged in");
            NSLog(@"name = %@ count =%ld",ar,[ar count]);
            
            [self performSegueWithIdentifier:@"login" sender:self];
        }
        else
        {
            NSLog(@"Invalid username or password");
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Login Error"
                                          message:@"invalid Username or Password"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Ok"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            [alert addAction:ok];
            //  [self presentViewController:alert animated:YES completion:nil];
            
        }
    }
    
    
}

@end
