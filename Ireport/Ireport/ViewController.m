//
//  ViewController.m
//  Ireport
//
//  Created by MDMARCO on 06/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "ViewController.h"
#import "UIImageView+WebCache.h"
#import "HomeCollectionViewCell.h"
#import "HomeCollectionReusableView.h"

@interface ViewController ()
{
    NSArray *image,*text,*color,*scrollimage;
    NSString *str1;
    CGFloat width;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    width = [UIScreen mainScreen].bounds.size.width;
    scrollimage=@[@"h1.png",@"h3.png",@"h4.png",@"h5.png",@"h6.png",@"h7.png",@"h8.png",@"h9.png",@"h10.png",@"h11.png",@"h12.png",@"h13.png"];
    image=@[@"13.png",@"9.png",@"17.png",@"2.png",@"18.png",@"4.png",@"6.png",@"10.png",@"3.png",@"16.png",@"15.png",@"14.png",@"12.png",@"7.png",@"11.png",@"1.png",@"1.png",@"1.png",@"5.png"];
    text=@[@"Report",@"Message",@"Teacher",@"Attendence",@"Time Table",@"Academic Calender",@"Fees",@"News",@"Bus",@"Study Files",@"Store",@"Services",@"Notes & Alarm",@"Grade & Behaviour",@"Notification",@"Parent",@"Government Information",@"About Us",@""];
    color=@[@"green.png",@"blue.png",@"rose.png",@"rred.png",@"green.png",@"blue.png",@"rose.png",@"rred.png",@"green.png",@"blue.png",@"rose.png",@"rred.png",@"green.png",@"blue.png",@"rose.png",@"rred.png",@"green.png",@"blue.png",@""];

}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    // int width1=CGRectGetWidth(collectionView.frame);
    
    if(indexPath.item ==18)
    {
        return CGSizeMake(width-2,width);
    }
//    else if(indexPath.item==15)
//    {
//        return CGSizeMake(width-2,(2*width/3));
//    }
    else
    {
      return CGSizeMake((width/3)-10,(2*width/5));
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return CGSizeMake(CGRectGetWidth(self.home.bounds),(width/3)*2);
    }
    else
    {
        return CGSizeZero;
    }
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    if(indexPath.section==0)
    {
         [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    }
    else
    {
         [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    }
    
    return flowLayout;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 0;
    }
    else
    {
        return image.count;
    }
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *identifier = @"Cell";
    HomeCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    if(indexPath.section==0)
//    {
//        
//         [[cell cellimage]setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[scrollimage objectAtIndex:indexPath.item]]]];
//    }
    if(indexPath.section==1)
    {

            
        [[cell cellimage]setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[image objectAtIndex:indexPath.item]]]];
            [[cell cellLabel] setText:[text objectAtIndex:indexPath.item]];
        [[cell underlineimage]setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[color objectAtIndex:indexPath.item]]]];
        if(indexPath.item==18)
        {
            [[cell cellimage]setContentMode:UIViewContentModeScaleToFill];
           
        }

          
    }
        
    
    return cell;
    
  
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    
    if (kind == UICollectionElementKindSectionHeader )
    {
        if(indexPath.section==0)
        {
            HomeCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"homereusable" forIndexPath:indexPath];
            [[headerView ScholarCap]setImage:[UIImage imageNamed:[NSString stringWithFormat:@"422.png"]]];
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/DIARY/school/studentimage/10011001.png"]];
            [[headerView ScholarSmiley]sd_setImageWithURL:url];
           // [[headerView ScholarSmiley]setImage:[UIImage imageNamed:[NSString stringWithFormat:@"avatar.png"]]];
            [[headerView homeImage]setImage:[UIImage imageNamed:[NSString stringWithFormat:@"home.png"]]];
            
            dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
            dispatch_async(mainThreadQueue, ^{
                
                
            });
           
            reusableview = headerView;
        }
        
        
    }
    
    
    
    return reusableview;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.item == 0)
    {
        str1=@"report";
        [[NSUserDefaults standardUserDefaults]setObject:str1 forKey:@"abcd"];
        [self performSegueWithIdentifier:@"view0" sender:self];
    }
    if(indexPath.item == 1)
    {
        [self performSegueWithIdentifier:@"view1" sender:self];
    }
    if(indexPath.item == 2)
    {
        [self performSegueWithIdentifier:@"view2" sender:self];
    }
    else if(indexPath.item == 3)
    {
        [self performSegueWithIdentifier:@"view3" sender:self];
    }
    else if(indexPath.item == 4)
    {
        [self performSegueWithIdentifier:@"view4" sender:self];
    }

    else if(indexPath.item == 5)
    {
        [self performSegueWithIdentifier:@"view5" sender:self];
    }
    
    
    //view5
    else if(indexPath.item == 6)
    {
        [self performSegueWithIdentifier:@"view6" sender:self];
    }
    
    else if(indexPath.item == 7)
    {
        [self performSegueWithIdentifier:@"view7" sender:self];
    }
    else if(indexPath.item == 8)
    {
        [self performSegueWithIdentifier:@"view8" sender:self];
    }
    else if(indexPath.item == 9)
    {
         str1=@"studyfile";
          [[NSUserDefaults standardUserDefaults]setObject:str1 forKey:@"abcd"];
        [self performSegueWithIdentifier:@"view9" sender:self];
    }
    else if(indexPath.item == 10)
    {
        [self performSegueWithIdentifier:@"view10" sender:self];
    }
    else if(indexPath.item == 11)
    {
        [self performSegueWithIdentifier:@"view11" sender:self];
    }
    else if(indexPath.item == 17)
    {
        [self performSegueWithIdentifier:@"view15" sender:self];
    }

    else if(indexPath.item == 18)
    {
        [self performSegueWithIdentifier:@"view18" sender:self];
    }
    else if(indexPath.item == 13)
    {
        [self performSegueWithIdentifier:@"view13" sender:self];
    }

    else
    {
        NSLog(@"BAKI ONNUM AAYILLA VERUTHE CLICK CHEYTHONDIRUNNOLUM SAVAM");
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}


@end
