//
//  HomeCollectionViewCell.h
//  Ireport
//
//  Created by MDMARCO on 06/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *cellimage;
@property (strong, nonatomic) IBOutlet UILabel *cellLabel;
@property (strong, nonatomic) IBOutlet UIImageView *underlineimage;

@end
