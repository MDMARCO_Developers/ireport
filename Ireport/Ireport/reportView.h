//
//  reportView.h
//  Ireport
//
//  Created by MDMARCO on 04/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface reportView : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong,nonatomic)NSDictionary *subjectDic;
@property (strong,nonatomic)NSArray *subject_Id,*subject_Name,*sublang_Id,*sublang_Name;
@property (strong, nonatomic) IBOutlet UITableView *subject_Table;

@end
