//
//  StoreViewCell.h
//  Ireport
//
//  Created by MDMARCO on 14/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *itemImage;
@property (strong, nonatomic) IBOutlet UILabel *itemName;
@property (strong, nonatomic) IBOutlet UILabel *itemprice;

@end
