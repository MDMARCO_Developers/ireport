//
//  HomeCollectionReusableView.h
//  Ireport
//
//  Created by MDMARCO on 06/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionReusableView : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UILabel *lblStudentName;
@property (strong, nonatomic) IBOutlet UIImageView *homeImage;
@property (strong, nonatomic) IBOutlet UIImageView *ScholarSmiley;
@property (strong, nonatomic) IBOutlet UIImageView *ScholarCap;

@end
