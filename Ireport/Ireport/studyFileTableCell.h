//
//  studyFileTableCell.h
//  Ireport
//
//  Created by MDMARCO on 07/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface studyFileTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblsubjname;
@property (strong, nonatomic) IBOutlet UILabel *filename;

@end
