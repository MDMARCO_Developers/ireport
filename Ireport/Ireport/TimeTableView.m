//
//  TimeTableView.m
//  Ireport
//
//  Created by MDMARCO on 10/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "TimeTableView.h"
#import "TimetableCell.h"


@interface TimeTableView ()
{
    int number;
    CGFloat width,height;
    
    NSArray *weak_array,*timefromarray,*timetoarray,*subject,*sun,*mon,*tue,*wed,*thu,*fri,*sat,*final;
    NSDictionary *timetableDic;
    UISwipeGestureRecognizer *recognizer,*rightrecog;
}

@property (strong, nonatomic) IBOutlet UIView *timetable_View;

@end

@implementation TimeTableView


- (void)viewDidLoad;
{

    [super viewDidLoad];
    number=0;
    weak_array=@[@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",@"Sunday"];
    [self gettimetable];
    height=self.view.frame.size.height;
    self.pageindex.numberOfPages=weak_array.count;
    self.pageindex.currentPage=number;
    
    

    
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    rightrecog = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    
     recognizer.delegate=self;
    rightrecog.delegate=self;
    
    recognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    rightrecog.direction = UISwipeGestureRecognizerDirectionRight;
    
    [_timetable_View addGestureRecognizer:recognizer];
    [_timetable_View addGestureRecognizer:rightrecog];
    
    
}
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gestureRecognizer
{

   if(gestureRecognizer.direction == UISwipeGestureRecognizerDirectionLeft)
   {
        NSLog(@"left");
       if(number==weak_array.count)
       {
       number=0;
       }
       else
       {
           number++;
       }
       [self changeWeak];
   }
   else
   {
        NSLog(@"right");
       if(number==0)
       {
           number=weak_array.count-1;
       }
       else
       {
           number--;
       }
       [self changeWeak];
         //     [self targetForAction:@selector(changeWeak:) withSender:self];
   }
    
}
-(void)changeWeak
{
    if(number>-1 && number<weak_array.count)
    {
        self.weakday.text=[NSString stringWithFormat:@"%@",[weak_array objectAtIndex:number]];
        self.pageindex.currentPage=number;
        
    }
    else
    {
        number=0;
        self.pageindex.currentPage=number;
        self.weakday.text=[NSString stringWithFormat:@"%@",[weak_array objectAtIndex:number]];
    }
    [self.time_table reloadData];
}
-(void)gettimetable
{
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/timetable.php?Class=12&Division=b&Scode=1001"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  
                                  timetableDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  timefromarray=[timetableDic valueForKey:@"StartTime"];
                                  
                                  timetoarray=[timetableDic  valueForKey:@"EndTime"];
                            sun=[timetableDic  valueForKey:@"Sunday"];
                            mon=[timetableDic  valueForKey:@"Monday"]; tue=[timetableDic  valueForKey:@"Tuesday"]; wed=[timetableDic  valueForKey:@"Wednesday"];
                            thu=[timetableDic  valueForKey:@"Thursday"];
                            fri=[timetableDic  valueForKey:@"Friday"];
                            sat=[timetableDic  valueForKey:@"Saturday"];
                                  
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      NSLog(@"table dic = %ld  data = %@",[timetableDic count],timetableDic);
                                      final=[NSArray arrayWithObjects:mon,tue,wed,thu,fri,sat,sun, nil];
                                      NSLog(@"final array = %@",final);
                                      [self loaddata];
                                      
                                  });
                              }];
    
    [dt resume];
    

}
-(void)loaddata
{
    self.weakday.text=[NSString stringWithFormat:@"%@",[weak_array objectAtIndex:0]];
    [self.time_table reloadData];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return timetableDic.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TimetableCell *cell=[tableView dequeueReusableCellWithIdentifier:@"times" forIndexPath:indexPath];
    cell.timefrom.text=[NSString stringWithFormat:@"%@",[timefromarray objectAtIndex:indexPath.row]];
    cell.timeto.text=[NSString stringWithFormat:@"%@",[timetoarray objectAtIndex:indexPath.row]];
    cell.subject.text=[NSString stringWithFormat:@"%@",[[final objectAtIndex:number]objectAtIndex:indexPath.row]];
    return cell;
}
@end
