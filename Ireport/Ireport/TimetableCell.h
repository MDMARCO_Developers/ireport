//
//  TimetableCell.h
//  Ireport
//
//  Created by MDMARCO on 18/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimetableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *timefrom;
@property (strong, nonatomic) IBOutlet UILabel *timeto;
@property (strong, nonatomic) IBOutlet UILabel *subject;

@end
