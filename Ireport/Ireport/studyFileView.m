//
//  studyFileView.m
//  Ireport
//
//  Created by MDMARCO on 07/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "studyFileView.h"
#import "studyFileTableCell.h"

@interface studyFileView ()
{
    NSString *code,*classid,*subjId;
    NSDictionary *dictionaryFiles;
    NSArray *titles,*filename,*fid;
}

@end

@implementation studyFileView

- (void)viewDidLoad {
    [super viewDidLoad];
    code=[[NSUserDefaults standardUserDefaults]stringForKey:@""];
    classid=[[NSUserDefaults standardUserDefaults]stringForKey:@""];
    subjId=[[NSUserDefaults standardUserDefaults]stringForKey:@"lang_id"];
    [self subjects];
    
    
    
}

-(void)subjects
{
    //http://192.168.0.108/school/appdata/Student_Subject_list.php?Scode=1001&class_id=1&language_id=817626687
    
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/StudyFiles_List.php?Scode=1001&class_id=1&subject_id=%@",subjId]];
    NSLog(@"url Id   %@",url);
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                 dictionaryFiles=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  fid=[dictionaryFiles valueForKey:@"id"];
                                  titles=[dictionaryFiles valueForKey:@"title"];
                                  filename=[dictionaryFiles valueForKey:@"filename"];
                                  NSLog(@"dictionary  %@",dictionaryFiles);
                                  
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{
                                      
                                      
                                    [_tableview reloadData];
                                      
                                  });
                              }];
    [dt resume];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return fid.count;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    studyFileTableCell *cell=[tableView dequeueReusableCellWithIdentifier:@"studyfile" forIndexPath:indexPath];
    cell.lblsubjname.text=[NSString stringWithFormat:@"%@",[titles objectAtIndex:indexPath.section]];
    cell.filename.text=[NSString stringWithFormat:@"%@",[filename objectAtIndex:indexPath.section]];
    
    return cell;
}

@end
