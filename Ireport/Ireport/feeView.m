//
//  feeView.m
//  Ireport
//
//  Created by MDMARCO on 04/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "feeView.h"
#import "FeeTableViewCell.h"

@interface feeView ()
{
    NSDictionary *feedic;
    NSArray *matter,*matid,*feeamt;
}

@end

@implementation feeView

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self feedetails];
    
}
-(void)feedetails
{
    //http://192.168.0.108/school/appdata/fees.php?Scode=1001&class_id=1
    
    NSURLSession *ses=[NSURLSession sharedSession];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/school/appdata/fees.php?Scode=1001&class_id=1"]];
    NSURLSessionDataTask *dt=[ses dataTaskWithURL:url completionHandler:^(NSData *data,NSURLResponse *resp,NSError *err)
                              {
                                  feedic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                  matid=[feedic valueForKey:@"id"];
                                  matter=[feedic  valueForKey:@"matter"];
                                  feeamt=[feedic valueForKey:@"fees"];
                                  
                                  dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                  dispatch_async(mainThreadQueue, ^{

                                      [self.tableFee reloadData];
                                  });
                              }];
    
    [dt resume];
    
    

    
    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return matid.count;}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeeTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"fees" forIndexPath:indexPath];
    cell.titles.text=[NSString stringWithFormat:@"%@",[matter objectAtIndex:indexPath.section]];
    cell.feesamount.text=[NSString stringWithFormat:@"%@",[feeamt objectAtIndex:indexPath.section]];
    
    return cell;
}
@end
