//
//  reportTableViewCell.h
//  Ireport
//
//  Created by MDMARCO on 04/04/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface reportTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *subjectName;

@end
