//
//  teacherDetailsView.m
//  Ireport
//
//  Created by MDMARCO on 13/03/17.
//  Copyright © 2017 mdmarco. All rights reserved.
//

#import "teacherDetailsView.h"
#import "UIImageView+WebCache.h"


@interface teacherDetailsView ()

@end

@implementation teacherDetailsView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.teacherName.text=[[NSUserDefaults standardUserDefaults]stringForKey:@"name"];
    self.teacherSubject.text=[[NSUserDefaults standardUserDefaults]stringForKey:@"subject"];
    self.teacherNumber.text=[[NSUserDefaults standardUserDefaults]stringForKey:@"teacherMobile"];
    self.img=[[NSUserDefaults standardUserDefaults]stringForKey:@"pic"];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.0.108/DIARY/school/teacherimage/%@",self.img]];
    [self.teacherImg sd_setImageWithURL:imageUrl];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


- (IBAction)callButton:(id)sender {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.teacherNumber.text]]];
}
@end
